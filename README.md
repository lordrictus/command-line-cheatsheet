# command-line-cheatsheet

## Hard Drive Management
```
shred -vn 1 -s 10G /dev/sda_device
```
hdparam - adjusts accoustic management leve / head-parking

## Quick Rsync
```
rsync -av -e "ssh -T -c arcfour -o Compression=no"
```
## File Split and Combine
```
tar cvzf - filename | split -b 1K - prefix
cat prefix* | tar xzvf -
```
